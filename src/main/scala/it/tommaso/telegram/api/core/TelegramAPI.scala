package it.tommaso.telegram.api.core

import java.io.File
import java.net.URI
import java.nio.charset.Charset

import it.tommaso.telegram.api.core.objects._
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.{ HttpGet, HttpPost }
import org.apache.http.client.utils.URIBuilder
import org.apache.http.entity.ContentType
import org.apache.http.entity.mime.content.{ ByteArrayBody, StringBody }
import org.apache.http.entity.mime.{ HttpMultipartMode, MultipartEntityBuilder }
import org.apache.http.impl.client.HttpClients
import org.apache.http.{ HttpEntity, HttpResponse, StatusLine }
import play.api.libs.json.{ JsArray, JsLookupResult, JsValue, Json }

import scala.collection.mutable

/**
  * Created by Tommaso on 05/03/16.
  **/
class TelegramAPI(accessToken: String)
{
	val telegramURI: URI = new URI("https://api.telegram.org/bot" + accessToken + "/");
	val client: HttpClient = HttpClients.createDefault();

	def setWebHook(address: String, certificatePath: String): Unit =
	{
		val paramMap: Map[String, Object] = Map("url" -> address, "certificate" -> new File(certificatePath));
		post("setWebhook", paramMap);
	}


	/**
	  * Not working if setWebHook have been invoked (from telegram documentation).
	  *
	  * @param offset  offset from the last update recieved ( 0 is default)
	  * @param limit   number of updates to recieve (1 to 100, 100 is default)
	  * @param timeout waiting time (0 default)
	  */
	def getUpdates(offset: Integer = 0, limit: Integer = 100, timeout: Integer = 0): List[TelegramUpdate] =
	{
		val paramMap: Map[String, String] = Map("offset" -> offset.toString, "limit" -> limit.toString, "timeout" -> timeout.toString);
		val json: JsValue = get("getUpdates", paramMap)
		val result: JsLookupResult = json.\("result")
		if (result == null)
			return List()
		val jsArray = result.get.as[JsArray];
		val updates: mutable.MutableList[TelegramUpdate] = mutable.MutableList();
		for (elem: JsValue <- jsArray.value)
		{
			val update: TelegramUpdate = TelegramUpdate.fromJson(elem);
			updates += update;
		}
		return updates.toList;
	}

	def getFile(fileId: String): Unit =
	{

	}

	def getPropertyMap(chatId: Any, disableNotification: Any, replyToMessageId: Any): mutable.Map[String, Object] =
	{
		val map: mutable.Map[String, Object] = mutable.Map("chat_id" -> chatId.asInstanceOf[Object],
			"disable_notification" -> disableNotification.asInstanceOf[Object])
		if (replyToMessageId != null)
			map += "reply_to_message_id" -> replyToMessageId.asInstanceOf[Object]
		return map
	}

	def sendVoice(chatId: Int, voice: File, duration: Integer = null, disableNotification: Boolean = false,
	              replyToMessageId: Integer = null): TelegramMessage =
	{
		val map: mutable.Map[String, Object] = getPropertyMap(chatId, disableNotification, replyToMessageId)
		map += "voice" -> voice
		if (duration != null)
			map += "duration" -> duration
		return send(map)
	}

	private def send(params: mutable.Map[String, Object]): TelegramMessage =
	{
		var content: JsValue = this.post("sendMessage", params.toMap)
		if (!content.\("ok").get.as[Boolean])
			return null
		content = content.\("result").get
		TelegramMessage.fromJson(content)
	}

	//TODO missing replyMarkup parameter
	def sendMessage(chatId: Integer, text: String, parseMode: String = null,
	                disableWebPagePreview: Boolean = false, disableNotification: Boolean = false,
	                replyToMessageId: Integer = null): TelegramMessage =
	{
		val map: mutable.Map[String, Object] = getPropertyMap(chatId, disableNotification, replyToMessageId)
		map += "text" -> text.asInstanceOf[Object]
		map += "disable_web_page_preview" -> disableWebPagePreview.asInstanceOf[Object]
		if (parseMode != null)
			map += "parse_mode" -> parseMode.asInstanceOf[Object]
		send(map)
	}

	private def handleStatusCode(sl: StatusLine): Unit =
	{
		if (sl.getStatusCode >= 400 && sl.getStatusCode < 500)
			println(sl.getStatusCode + ":" + sl.getReasonPhrase)
		//throw new IllegalStateException(sl.getStatusCode + ":" + sl.getReasonPhrase);
	}

	def get(methodName: String, parameterDictionary: Map[String, String]): JsValue =
	{
		val uriBuilder: URIBuilder = new URIBuilder(telegramURI);
		uriBuilder.setPath(uriBuilder.getPath + methodName);
		for ((key, value) <- parameterDictionary)
			uriBuilder.addParameter(key, value);
		val get: HttpGet = new HttpGet(uriBuilder.build());
		val response: HttpResponse = client.execute(get);
		handleStatusCode(response.getStatusLine);
		print(uriBuilder.build())
		Json.parse(response.getEntity.getContent)
	}

	def post(methodName: String, parameterDictionary: Map[String, Object]): JsValue =
	{
		val entityBuilder: MultipartEntityBuilder = MultipartEntityBuilder.create()
		entityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE)
		for ((key, value) <- parameterDictionary)
		{
			value match
			{
				case f: File =>
				{
					val source = scala.io.Source.fromFile(f.getAbsolutePath)
					val byteArray = source.map(_.toByte).toArray
					source.close()
					entityBuilder.addPart(key, new ByteArrayBody(byteArray, ContentType.MULTIPART_FORM_DATA, f.getName)) //new FileBody(f, ContentType.DEFAULT_BINARY))
				};
				case _ => entityBuilder.addPart(key, new StringBody(value.toString, ContentType.create("text/plain", Charset.forName("utf-8"))))
			}
		}
		val httpEntity: HttpEntity = entityBuilder.build()
		val uriBuilder: URIBuilder = new URIBuilder(telegramURI)
		uriBuilder.setPath(uriBuilder.getPath + methodName)
		val post: HttpPost = new HttpPost(uriBuilder.build())
		post.setEntity(httpEntity)
		val response: HttpResponse = client.execute(post)
		handleStatusCode(response.getStatusLine)
		Json.parse(response.getEntity.getContent)
	}
}

//{
//	def main(args: Array[String])
//	{
//		val client: TelegramAPI = new TelegramAPI("163346100:AAGHk0KDInDb0WEwykUyX8aLHc5NH7wl0ao")
//		val updates: List[TelegramUpdate] = client.getUpdates(49144964, 100, 1000);
//		for (tu: TelegramUpdate <- updates)
//		{
//			println(tu.toString)
//			val text: String = tu.getMessage().getText
//			if (text.toLowerCase().contains("parla"))
//			{
//				client.sendMessage(tu.getMessage().getChat.getId(), "parla con sto cazzo");
//			}
//		}
//	}
//}
