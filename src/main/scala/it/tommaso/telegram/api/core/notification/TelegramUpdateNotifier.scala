package it.tommaso.telegram.api.core.notification
/**
 * Created by Tommaso on 05/03/16.
 */
trait TelegramUpdateNotifier {
  def runNotificationSystem(): Unit;
  def addListener(listener: TelegramListener): Boolean;
  def removeListener(listener: TelegramListener): Boolean;
  def stop(): Unit;

}
