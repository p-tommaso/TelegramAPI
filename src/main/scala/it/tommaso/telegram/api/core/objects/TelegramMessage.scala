package it.tommaso.telegram.api.core.objects

import java.lang

import play.api.libs.json.{JsArray, JsString, JsValue}

import scala.collection.mutable

/**
 * Created by Tommaso on 05/03/16.
 */
class TelegramMessage(id: Integer, from: TelegramUser, date: Long, text: String,
  chat: TelegramChat, photos: List[TelegramPhotoRef]) {
  def getText(): String = text

  def getFrom(): TelegramUser = from

  def getChat(): TelegramChat = chat

  def hasPhotos(): Boolean = photos != null && !photos.isEmpty

  def getDate() : Long = date * 1000 // the parsed date omits milliseconds info so have to be multipled by 1000
  override def toString(): String = from.toString + text
}

object TelegramMessage {
  def fromJson(json: JsValue): TelegramMessage =
    {
      val list: List[JsValue] = json.\("photo").getOrElse(JsArray()).as[List[JsValue]]
      val photos: mutable.MutableList[TelegramPhotoRef] = mutable.MutableList()
      for (value: JsValue <- list) {
        val photoref: TelegramPhotoRef = TelegramPhotoRef.fromJson(value)
        photos += photoref
      }

      return new TelegramMessage(
        Integer.parseInt(json.\("message_id").get.toString()),
        TelegramUser.fromJson(json.\("from").getOrElse(JsString(""))),
        lang.Long.parseLong(json.\("date").get.toString()),
        json.\("text").getOrElse(JsString("")).as[String],
        TelegramChat.fromJson(json.\("chat").get), photos.toList
      )
    }
}
