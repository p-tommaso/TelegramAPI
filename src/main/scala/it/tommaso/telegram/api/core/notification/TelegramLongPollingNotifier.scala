package it.tommaso.telegram.api.core.notification
import java.util.concurrent.atomic.AtomicBoolean
import it.tommaso.telegram.api.core.TelegramAPI
import it.tommaso.telegram.api.core.objects.TelegramUpdate
import scala.collection.mutable

/**
 * Created by Tommaso on 05/03/16.
 */
class TelegramLongPollingNotifier(telegramApi: TelegramAPI, updateLimit: Integer = 100, timeout: Integer = 60) extends TelegramUpdateNotifier {
  val listeners: mutable.LinkedHashSet[TelegramListener] = mutable.LinkedHashSet();
  val stopNotificationSystem: AtomicBoolean = new AtomicBoolean(true);
  //	val telegramApi: TelegramAPI = new TelegramAPI(accessToken);

  override def runNotificationSystem(): Unit =
    {
      stopNotificationSystem.set(false);
      println("Starting notification system")
      println("there are " + listeners.size + " listeners:")
      listeners.foreach(x => println(x))
      var lastUpdate: Integer = -1 //XXX should save this on a file so if it crash i can recover
      while (!stopNotificationSystem.get()) {
        val updates: List[TelegramUpdate] = telegramApi.getUpdates(lastUpdate + 1, updateLimit, timeout)
        println("read " + updates.size + " updates")
        if (!updates.isEmpty) {
          val last: TelegramUpdate = updates.last
          lastUpdate = last.getId()
          for (listener: TelegramListener <- listeners)
            listener.notify(updates)
        }
      }
      println("notification system started")
    }

  override def stop(): Unit =
    {
      stopNotificationSystem.set(true)
    }

  override def removeListener(listener: TelegramListener): Boolean =
    {
      listeners.remove(listener)
    }

  override def addListener(listener: TelegramListener): Boolean =
    {
      listeners.add(listener)
    }
}
