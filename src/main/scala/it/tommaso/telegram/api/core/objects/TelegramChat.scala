package it.tommaso.telegram.api.core.objects

import play.api.libs.json.{JsString, JsValue}

/**
  * Created by Tommaso on 05/03/16.
  */
class TelegramChat(id:Integer, chatType:String, title:String, username:String, firstName:String, lastName:String)
{
	def getId() : Integer=id
}

object TelegramChat
{
	def fromJson(json:JsValue):TelegramChat=
	{
		return new TelegramChat(Integer.parseInt(json.\("id").get.toString()),
			json.\("type").get.as[String],
			json.\("title").getOrElse(JsString("")).as[String],
			json.\("username").getOrElse(JsString("")).as[String],
			json.\("first_name").getOrElse(JsString("")).as[String],
			json.\("last_name").getOrElse(JsString("")).as[String]);
	}
}
