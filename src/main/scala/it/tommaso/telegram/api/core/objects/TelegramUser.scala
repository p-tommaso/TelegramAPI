package it.tommaso.telegram.api.core.objects

import play.api.libs.json.{ JsString, JsValue }

/**
 * Created by Tommaso on 05/03/16.
 */
class TelegramUser(id: Integer, firstName: String, lastName: String, userName: String) {
  def getId: Integer = id
  def getName: String = firstName
  
  override def toString(): String =
    {
      val builder: StringBuilder = new StringBuilder(firstName)
      if (!lastName.isEmpty)
        builder.append(" ").append(lastName);
      if (!userName.isEmpty)
        builder.append("(").append(userName).append(")");
      builder.append(": ");
      return builder.toString();
    }
}

object TelegramUser {
  def fromJson(json: JsValue): TelegramUser =
    {
      if (json.toString().isEmpty)
        return null;
      return new TelegramUser(
        Integer.parseInt(json.\("id").get.toString()),
        json.\("first_name").get.as[String],
        json.\("last_name").getOrElse(JsString("")).as[String],
        json.\("username").getOrElse(JsString("")).as[String]
      );
    }
}
