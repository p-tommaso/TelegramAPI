package it.tommaso.telegram.api.core.objects

import play.api.libs.json.{JsUndefined, JsLookupResult, JsValue}

/**
  * Created by Tommaso on 05/03/16.
  */
class TelegramUpdate(id:Integer, message:TelegramMessage)
{
	def getId(): Integer =
	{
		return id;
	}

	def getMessage(): TelegramMessage=
	{
		return message;
	}

	override def toString(): String =
	{
		return "Update: " + id.toString + "\n" + message;
	}
}

object TelegramUpdate
{
	def fromJson(jsValue: JsValue):TelegramUpdate=
	{
		val res : JsLookupResult = jsValue.\("message")
		if(res.getClass == JsUndefined.getClass)
			return new TelegramUpdate(Integer.parseInt(jsValue.\("update_id").get.toString()), null)

		new TelegramUpdate(Integer.parseInt(jsValue.\("update_id").get.toString()),
			TelegramMessage.fromJson(jsValue.\("message").as[JsValue]));
	}
}
