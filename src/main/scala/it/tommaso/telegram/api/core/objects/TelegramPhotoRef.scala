package it.tommaso.telegram.api.core.objects

import play.api.libs.json.{ JsValue, JsString }

/**
 * Created by Tommaso on 05/03/16.
 */
class TelegramPhotoRef(fileId: String, width: Integer, height: Integer, size: Integer = null) {
  def getFileId() = fileId

  def getWidth() = width

  def getHeight() = height

  def getSize() = size
}

object TelegramPhotoRef {
  def fromJson(json: JsValue): TelegramPhotoRef =
    {
      new TelegramPhotoRef(
        json.\("file_id").get.as[String],
        Integer.parseInt(json.\("width").get.toString()),
        Integer.parseInt(json.\("height").get.toString()),
        Integer.parseInt(json.\("file_size").getOrElse(JsString("0")).toString())
      )
    }
}
