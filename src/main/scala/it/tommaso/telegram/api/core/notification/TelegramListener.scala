package it.tommaso.telegram.api.core.notification

import it.tommaso.telegram.api.core.objects.TelegramUpdate
/**
  * Created by Tommaso on 05/03/16.
  */
trait TelegramListener
{
	def notify(telegramUpdate: List[TelegramUpdate]):Unit
}
